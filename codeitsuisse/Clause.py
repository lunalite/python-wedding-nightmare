class Clause:
    def __init__(self, literals=None):
        self.cached_positive_symbols = set()
        self.cached_negative_symbols = set()
        self.cached_tautology_result = None
        if literals:
            self.literals = literals
            for literal in literals:
                if not literal.is_negation:
                    self.cached_positive_symbols.add(literal.symbol)
                else:
                    self.cached_negative_symbols.add(literal.symbol)
        else:
            self.literals = list()

    def is_empty(self):
        return len(self.literals) == 0

    def is_false(self):
        return self.is_empty()

    def is_tautology(self):
        if self.cached_tautology_result is None:
            if len(self.cached_positive_symbols.intersection(self.cached_negative_symbols)) >= 1:
                self.cached_tautology_result = True
                return True
            else:
                self.cached_tautology_result = False
                return False
        else:
            return self.cached_tautology_result

    def add_literal(self, literal):
        self.literals.append(literal)
        if not literal.is_negation:
            self.cached_positive_symbols.add(literal.symbol)
        else:
            self.cached_negative_symbols.add(literal.symbol)
