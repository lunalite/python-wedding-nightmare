import json
from datetime import datetime

from codeitsuisse.DPLL import DPLL
from codeitsuisse.Logic import Logic

logic_info = Logic()


def execute_own_solution(test_data, expected_result):
    for datum in test_data:
        friends = datum['friends']
        enemies = datum['enemies']
        families = datum['families']

        tables_seats = logic_info.generate_table()
        enemies = logic_info.generate_enemies_clauses(enemies)
        families = logic_info.generate_family_friend_clauses(families)
        friends = logic_info.generate_family_friend_clauses(friends)

        information = set()
        information.update(tables_seats)
        information.update(enemies)
        information.update(families)
        information.update(friends)

        dpll = DPLL(information)
        dpll_satisfiable = dpll.dpll_satisfiable()

        # f = open('./output/output.txt', "w")
        # f.write(str(dpll_satisfiable))
        # f.close()

        expected_result.append(dpll_satisfiable)
    return expected_result


if __name__ == '__main__':
    startTime = datetime.now()

    with open('./input/input.json') as json_file:
        data = json.load(json_file)
    execute_own_solution(data)

    print(datetime.now() - startTime)
