import logging

from flask import request, jsonify;

from codeitsuisse import app;
from codeitsuisse.DPLL import DPLL
from codeitsuisse.Logic import Logic

logger = logging.getLogger(__name__)


@app.route('/wedding-nightmare', methods=['POST'])
def evaluate():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = execute_own_solution(data)
    logging.info("My result :{}".format(result))
    return jsonify(result)


def execute_own_solution(test_data):
    expected_result = []
    for datum in test_data:
        _id = datum['_id']
        tables = datum['tables']
        guests = datum['guests']
        friends = datum['friends']
        enemies = datum['enemies']
        families = datum['families']

        logic_info = Logic(tables, guests)

        tables_seats = logic_info.generate_table()
        enemies = logic_info.generate_enemies_clauses(enemies)
        families = logic_info.generate_family_friend_clauses(families)
        friends = logic_info.generate_family_friend_clauses(friends)

        information = set()
        information.update(tables_seats)
        information.update(enemies)
        information.update(families)
        information.update(friends)

        dpll = DPLL(information)
        dpll_satisfiable = dpll.dpll_satisfiable()

        if dpll_satisfiable:
            assignment = dpll.model.assignments
            table_assignment = [k for k, v in assignment.items() if v is True]

        expected_result.append(
            {
                "_id": _id,
                "satisfiable": dpll_satisfiable,
                "allocation": table_assignment if dpll_satisfiable else {}
            }
        )
    for i in expected_result:
        print(i)

    return expected_result
