from flask import jsonify


def methodCall(list):
    # implementation
    list = [(x + 1) % 2 for x in list]
    return jsonify({
        't': list
    })
